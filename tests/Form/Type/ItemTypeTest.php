<?php
declare(strict_types=1);

namespace App\Tests\Form\Type;

use App\Entity\Item\BaseItem;
use App\Factory\Item\FormBaseItemFactory;
use App\Form\Type\Item\ItemType;
use Symfony\Component\Form\Test\TypeTestCase;

class ItemTypeTest extends TypeTestCase
{
    /**
     * @dataProvider provideItem
     * @param array $formData
     */
    public function testSubmitValidData(array $formData): void
    {
        $baseItemFactory = new FormBaseItemFactory();
        $itemToCompare = $baseItemFactory->createBaseItem();
        $form = $this->factory->create(ItemType::class, $itemToCompare);
        $item = (new BaseItem())
            ->setName($formData['name'])
            ->setAmount($formData['amount']);

        $form->submit($formData);
        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($item, $itemToCompare);

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }

    /**
     * @return array
     */
    public function provideItem(): array
    {
        return [
            [
                [
                    'name' => 'test name',
                    'amount' => '5'
                ]
            ]
        ];
    }
}
