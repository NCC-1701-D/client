<?php
declare(strict_types=1);

namespace App\Tests\Service\ApiClient;

use App\Service\ApiClient\RequestOptionsComposer;
use PHPUnit\Framework\TestCase;

class RequestOptionsComposerTest extends TestCase
{
    /**
     * @dataProvider provideGetOptions
     * @param array $getOptions
     * @param array $queryParams
     */
    public function testComposeGetOptions(array $getOptions, array $queryParams): void
    {
        $getOptionsToCompare = (new RequestOptionsComposer())->composeGetOptions($queryParams);
        $this->assertEquals($getOptions, $getOptionsToCompare);
    }

    /**
     * @dataProvider providePostOptions
     * @param array $postOptions
     * @param string $body
     */
    public function testComposePostOptions(array $postOptions, string $body): void
    {
        $postOptionsToCompare = (new RequestOptionsComposer())->composePostOptions($body);
        $this->assertEquals($postOptions, $postOptionsToCompare);
    }

    /**
     * @dataProvider providePatchOptions
     * @param array $patchOptions
     * @param string $body
     */
    public function testComposePatchOptions(array $patchOptions, string $body): void
    {
        $patchOptionsToCompare = (new RequestOptionsComposer())->composePatchOptions($body);
        $this->assertEquals($patchOptions, $patchOptionsToCompare);
    }

    /**
     * @dataProvider provideDeleteOptions
     * @param array $deleteOptions
     */
    public function testComposeDeleteOptions(array $deleteOptions): void
    {
        $deleteOptionsToCompare = (new RequestOptionsComposer())->composeDeleteOptions();
        $this->assertEquals($deleteOptions, $deleteOptionsToCompare);
    }

    /**
     * @return array
     */
    public function provideGetOptions(): array
    {
        $queryParams = [];

        return [
            [
                $options = [
                    'headers' => ['accept' => 'application/json'],
                    'query' => $queryParams
                ],
                $queryParams
            ]
        ];
    }

    /**
     * @return array
     */
    public function providePostOptions(): array
    {
        $body = '';

        return [
            [
                [
                    'headers' => [
                        'accept' => 'application/json',
                        'Content-Type' => 'application/json'
                    ],
                    'body' => $body
                ],
                $body
            ]
        ];
    }

    /**
     * @return array
     */
    public function providePatchOptions(): array
    {
        $body = '';

        return [
            [
                [
                    'headers' => [
                        'accept' => 'application/json',
                        'Content-Type' => 'application/merge-patch+json'
                    ],
                    'body' => $body
                ],
                $body
            ]
        ];
    }

    /**
     * @return array
     */
    public function provideDeleteOptions(): array
    {
        return [
            [
                [
                    'headers' => ['accept' => '*/*']
                ]
            ]
        ];
    }
}
