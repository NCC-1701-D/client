<?php
declare(strict_types=1);

namespace App\Tests\Service\ApiClient;

use App\Exception\ApiException;
use App\Factory\ApiClient\ApiRequestFactory;
use App\Service\ApiClient\ApiClient;
use App\Service\ApiClient\RequestOptionsComposer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\HttpFoundation\Response;

class ApiClientTest extends TestCase
{
    protected static string $testUrl = 'http://test.url/';
    protected static string $testBody= 'test body';
    protected RequestOptionsComposer $requestOptionsComposer;
    protected ApiRequestFactory $apiRequestFactory;

    protected function setUp(): void
    {
        $this->requestOptionsComposer = new RequestOptionsComposer();
        $this->apiRequestFactory = new ApiRequestFactory();
    }

    /**
     * @dataProvider provideGetData
     * @param MockHttpClient $httpClient
     * @param string $body
     * @throws ApiException
     */
    public function testGetMethod(MockHttpClient $httpClient, string $body): void
    {
        $apiClient = new ApiClient($httpClient, $this->requestOptionsComposer, $this->apiRequestFactory);
        $bodyToCompare = $apiClient->get(self::$testUrl);
        $this->assertEquals($body, $bodyToCompare);
    }

    /**
     * @dataProvider providePostData
     * @param MockHttpClient $httpClient
     * @param string $body
     * @throws ApiException
     */
    public function testPostMethod(MockHttpClient $httpClient, string $body): void
    {
        $apiClient = new ApiClient($httpClient, $this->requestOptionsComposer, $this->apiRequestFactory);
        $bodyToCompare = $apiClient->post(self::$testUrl, $body);
        $this->assertEquals($body, $bodyToCompare);
    }

    /**
     * @dataProvider providePatchData
     * @param MockHttpClient $httpClient
     * @param string $body
     * @throws ApiException
     */
    public function testPatchMethod(MockHttpClient $httpClient, string $body): void
    {
        $apiClient = new ApiClient($httpClient, $this->requestOptionsComposer, $this->apiRequestFactory);
        $bodyToCompare = $apiClient->patch(self::$testUrl, $body);
        $this->assertEquals($body, $bodyToCompare);
    }

    /**
     * @dataProvider provideDeleteData
     * @param MockHttpClient $httpClient
     * @param string $body
     * @throws ApiException
     */
    public function testDeleteMethod(MockHttpClient $httpClient, string $body): void
    {
        $apiClient = new ApiClient($httpClient, $this->requestOptionsComposer, $this->apiRequestFactory);
        $bodyToCompare = $apiClient->delete(self::$testUrl);
        $this->assertEquals($body, $bodyToCompare);
    }

    /**
     * @return array
     */
    public function provideGetData(): array
    {
        $body = self::$testBody;

        return [
            [
                new MockHttpClient(
                    new MockResponse(
                        $body,
                        ['http_code' => Response::HTTP_OK]
                    )
                ),
                $body
            ]
        ];
    }

    /**
     * @return array
     */
    public function providePostData(): array
    {
        $body = self::$testBody;

        return [
            [
                new MockHttpClient(
                    new MockResponse(
                        self::$testBody,
                        ['http_code' => Response::HTTP_CREATED]
                    )
                ),
                $body
            ]
        ];
    }

    /**
     * @return array
     */
    public function providePatchData(): array
    {
        $body = self::$testBody;

        return [
            [
                new MockHttpClient(
                    new MockResponse(
                        self::$testBody,
                        ['http_code' => Response::HTTP_OK]
                    )
                ),
                $body
            ]
        ];
    }

    /**
     * @return array
     */
    public function provideDeleteData(): array
    {
        $body = self::$testBody;

        return [
            [
                new MockHttpClient(
                    new MockResponse(
                        $body,
                        ['http_code' => Response::HTTP_NO_CONTENT]
                    )
                ),
                $body
            ]
        ];
    }
}
