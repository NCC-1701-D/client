<?php
declare(strict_types=1);

namespace App\Tests\Service\Item;

use App\Entity\Item\BaseItem;
use App\Entity\Item\Item;
use App\Exception\DataSourceException;
use App\Service\ApiClient\ApiClientInterface;
use App\Service\Item\ApiItemService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\SerializerInterface;

class ApiItemServiceTest extends TestCase
{
    protected static string $testUrl = 'http://test.url/';

    /**
     * @dataProvider provideItem
     * @param Item $item
     * @throws DataSourceException
     */
    public function testGetItem(Item $item): void
    {
        $apiClientMock = $this->createMock(ApiClientInterface::class);
        $apiClientMock
            ->expects($this->once())
            ->method('get');
        $serializerMock = $this->createMock(SerializerInterface::class);
        $serializerMock
            ->expects($this->once())
            ->method('deserialize')
            ->willReturn($item);
        $itemService = new ApiItemService($apiClientMock, $serializerMock, self::$testUrl);
        $itemToCompare = $itemService->getItem(1);
        $this->assertEquals($item, $itemToCompare);
    }

    /**
     * @dataProvider provideItems
     * @param array $items
     * @throws DataSourceException
     */
    public function testGetItems(array $items): void
    {
        $apiClientMock = $this->createMock(ApiClientInterface::class);
        $apiClientMock
            ->expects($this->once())
            ->method('get');
        $serializerMock = $this->createMock(SerializerInterface::class);
        $serializerMock
            ->expects($this->once())
            ->method('deserialize')
            ->willReturn($items);
        $itemService = new ApiItemService($apiClientMock, $serializerMock, self::$testUrl);
        $itemsToCompare = $itemService->getItems();
        $this->assertEquals($items, $itemsToCompare);
    }

    /**
     * @dataProvider provideBaseItem
     * @param BaseItem $item
     * @throws DataSourceException
     */
    public function testCreateItem(BaseItem $item): void
    {
        $apiClientMock = $this->createMock(ApiClientInterface::class);
        $apiClientMock
            ->expects($this->once())
            ->method('post');
        $serializerMock = $this->createMock(SerializerInterface::class);
        $serializerMock
            ->expects($this->once())
            ->method('serialize')
            ->willReturn('');
        $itemService = new ApiItemService($apiClientMock, $serializerMock, self::$testUrl);
        $itemService->createItem($item);
    }

    /**
     * @dataProvider provideItem
     * @param Item $item
     * @throws DataSourceException
     */
    public function testUpdateItem(Item $item): void
    {
        $apiClientMock = $this->createMock(ApiClientInterface::class);
        $apiClientMock
            ->expects($this->once())
            ->method('patch');
        $serializerMock = $this->createMock(SerializerInterface::class);
        $serializerMock
            ->expects($this->once())
            ->method('serialize')
            ->willReturn('');
        $itemService = new ApiItemService($apiClientMock, $serializerMock, self::$testUrl);
        $itemService->updateItem(1, $item);
    }

    /**
     * @throws DataSourceException
     */
    public function testDeleteItem(): void
    {
        $apiClientMock = $this->createMock(ApiClientInterface::class);
        $apiClientMock
            ->expects($this->once())
            ->method('delete');
        $serializerMock = $this->createMock(SerializerInterface::class);
        $itemService = new ApiItemService($apiClientMock, $serializerMock, self::$testUrl);
        $itemService->deleteItem(1);
    }

    /**
     * @return array
     */
    public function provideItems(): array
    {
        return [
            [
                [
                    (new Item())
                        ->setId(1)
                        ->setName('test')
                        ->setAmount(2)
                ]
            ]
        ];
    }

    /**
     * @return array
     */
    public function provideBaseItem(): array
    {
        return [
            [
                (new BaseItem())
                    ->setName('test')
                    ->setAmount(2)
            ]
        ];
    }

    /**
     * @return array
     */
    public function provideItem(): array
    {
        return [
            [
                (new Item())
                    ->setId(1)
                    ->setName('test')
                    ->setAmount(2)
            ]
        ];
    }
}
