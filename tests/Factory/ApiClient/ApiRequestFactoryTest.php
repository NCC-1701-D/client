<?php
declare(strict_types=1);

namespace App\Tests\Factory\ApiClient;

use App\Entity\ApiClient\ApiRequest;
use App\Factory\ApiClient\ApiRequestFactory;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiRequestFactoryTest extends TestCase
{
    protected static string $testUrl = 'http://test.url/';

    /**
     * @dataProvider provideGetApiRequest
     * @param ApiRequest $apiRequest
     * @param string $url
     * @param array $options
     */
    public function testApiRequestGetRequest(ApiRequest $apiRequest, string $url, array $options): void
    {
        $getApiRequestToCompare = (new ApiRequestFactory())->buildGetApiRequest($url, $options);
        $this->assertEquals($apiRequest, $getApiRequestToCompare);

    }

    /**
     * @dataProvider providePostApiRequest
     * @param ApiRequest $apiRequest
     * @param string $url
     * @param array $options
     */
    public function testApiRequestPostRequest(ApiRequest $apiRequest, string $url, array $options): void
    {
        $postApiRequestToCompare = (new ApiRequestFactory())->buildPostApiRequest($url, $options);
        $this->assertEquals($apiRequest, $postApiRequestToCompare);
    }


    /**
     * @dataProvider providePatchApiRequest
     * @param ApiRequest $apiRequest
     * @param string $url
     * @param array $options
     */
    public function testApiRequestPatchRequest(ApiRequest $apiRequest, string $url, array $options): void
    {
        $patchApiRequestToCompare = (new ApiRequestFactory())->buildPatchApiRequest($url, $options);
        $this->assertEquals($apiRequest, $patchApiRequestToCompare);
    }

    /**
     * @dataProvider provideDeleteApiRequest
     * @param ApiRequest $apiRequest
     * @param string $url
     * @param array $options
     */
    public function testApiRequestDeleteRequest(ApiRequest $apiRequest, string $url, array $options): void
    {
        $deleteApiRequestToCompare = (new ApiRequestFactory())->buildDeleteApiRequest($url, $options);
        $this->assertEquals($apiRequest, $deleteApiRequestToCompare);
    }

    /**
     * @return array
     */
    public function provideGetApiRequest(): array
    {
        $url = self::$testUrl;
        $options = [];

        return [
            [
                (new ApiRequest())
                    ->setUrl($url)
                    ->setMethod(Request::METHOD_GET)
                    ->setOptions($options)
                    ->setAllowedStatus(Response::HTTP_OK),
                $url,
                $options
            ]
        ];
    }

    /**
     * @return array
     */
    public function providePostApiRequest(): array
    {
        $url = self::$testUrl;
        $options = [];

        return [
            [
                (new ApiRequest())
                    ->setUrl($url)
                    ->setMethod(Request::METHOD_POST)
                    ->setOptions($options)
                    ->setAllowedStatus(Response::HTTP_CREATED),
                $url,
                $options
            ]
        ];
    }

    /**
     * @return array
     */
    public function providePatchApiRequest(): array
    {
        $url = self::$testUrl;
        $options = [];

        return [
            [
                (new ApiRequest())
                    ->setUrl($url)
                    ->setMethod(Request::METHOD_PATCH)
                    ->setOptions($options)
                    ->setAllowedStatus(Response::HTTP_OK),
                $url,
                $options
            ]
        ];
    }

    /**
     * @return array
     */
    public function provideDeleteApiRequest(): array
    {
        $url = self::$testUrl;
        $options = [];

        return [
            [
                (new ApiRequest())
                    ->setUrl($url)
                    ->setMethod(Request::METHOD_DELETE)
                    ->setOptions($options)
                    ->setAllowedStatus(Response::HTTP_NO_CONTENT),
                $url,
                $options
            ]
        ];
    }
}
