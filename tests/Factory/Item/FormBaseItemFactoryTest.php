<?php
declare(strict_types=1);

namespace App\Tests\Factory\Item;

use App\Entity\Item\BaseItem;
use App\Factory\Item\FormBaseItemFactory;
use Monolog\Test\TestCase;

class FormBaseItemFactoryTest extends TestCase
{
    public function testCreatedItem(): void
    {
        $baseItemFactory = new FormBaseItemFactory();
        $itemToCompare = $baseItemFactory->createBaseItem();
        $item = (new BaseItem())
            ->setName('')
            ->setAmount(0);

        $this->assertEquals($item, $itemToCompare);
    }
}
