# Client

## Development

### Setting up dev environment

1. Set up environment variables:

        cp .env.dist .env

2. Run docker containers:

        docker-compose up -d --build
    
3. Log into client container:

        docker exec -it client /bin/bash

4. Install dependencies(in container):

        composer install && yarn install
        
5. Build assets(in container):
        
        yarn encore dev
    
5. Clear cache(in container):

        php bin/console cache:clear
        
To run tests(in container):

    php bin/phpunit
        