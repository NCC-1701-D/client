<?php
declare(strict_types=1);

namespace App\Service\Item;

use App\Entity\Item\BaseItem;
use App\Exception\DataSourceException;

interface CreateItemInterface
{
    /**
     * @param BaseItem $newItem
     * @throws DataSourceException
     */
    public function createItem(BaseItem $newItem): void;
}
