<?php
declare(strict_types=1);

namespace App\Service\Item;

use App\Entity\Item\Item;
use App\Exception\DataSourceException;

interface ItemDataSourceInterface
{
    /**
     * @return Item[]
     * @throws DataSourceException
     */
    public function getItemsThatAreInStock(): array;

    /**
     * @return Item[]
     * @throws DataSourceException
     */
    public function getItemsThatAreNotInStock(): array;

    /**
     * @return Item[]
     * @throws DataSourceException
     */
    public function getItemsThatAreMoreThan5(): array;

    /**
     * @return Item[]
     * @throws DataSourceException
     */
    public function getItems(): array;

    /**
     * @param int $id
     * @return Item
     * @throws DataSourceException
     */
    public function getItem(int $id): object;
}
