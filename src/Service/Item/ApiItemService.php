<?php
declare(strict_types=1);

namespace App\Service\Item;

use App\Entity\Item\BaseItem;
use App\Entity\Item\Item;
use App\Exception\ApiException;
use App\Exception\DataSourceException;
use App\Service\ApiClient\ApiClientInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class ApiItemService implements ItemDataSourceInterface, CreateItemInterface, UpdateItemInterface, DeleteItemInterface
{
    const API_RESOURCE = '/items';

    private ApiClientInterface $apiClient;
    private SerializerInterface $serializer;
    private string $apiHost;

    /**
     * @param ApiClientInterface $apiClient
     * @param SerializerInterface $serializer
     * @param string $apiHost
     */
    public function __construct(ApiClientInterface $apiClient, SerializerInterface $serializer, string $apiHost)
    {
        $this->apiClient = $apiClient;
        $this->serializer = $serializer;
        $this->apiHost = $apiHost;
    }

    /**
     * @return Item[]
     * @throws DataSourceException
     */
    public function getItemsThatAreInStock(): array
    {
        return $this->getItems(['amount[gt]' => 0]);
    }

    /**
     * @return Item[]
     * @throws DataSourceException
     */
    public function getItemsThatAreNotInStock(): array
    {
        return $this->getItems(['amount[lt]' => 1]);
    }

    /**
     * @return Item[]
     * @throws DataSourceException
     */
    public function getItemsThatAreMoreThan5(): array
    {
        return $this->getItems(['amount[gt]' => 5]);
    }

    /**
     * @param int $id
     * @return Item
     * @throws DataSourceException
     */
    public function getItem(int $id): object
    {
        try {
            $content = $this->apiClient->get($this->getApiUrl($id));
        } catch (ApiException $e) {
            throw new DataSourceException($e->getMessage());
        }

        return $this->serializer->deserialize($content, Item::class, JsonEncoder::FORMAT);
    }

    /**
     * @param int $id
     * @throws DataSourceException
     */
    public function deleteItem(int $id): void
    {
        try {
            $this->apiClient->delete($this->getApiUrl($id));
        } catch (ApiException $e) {
            throw new DataSourceException($e->getMessage());
        }
    }

    /**
     * @param BaseItem $newItem
     * @throws DataSourceException
     */
    public function createItem(BaseItem $newItem): void
    {
        $body = $this->serializer->serialize($newItem, JsonEncoder::FORMAT);

        try {
            $this->apiClient->post($this->getApiUrl(), $body);
        } catch (ApiException $e) {
            throw new DataSourceException($e->getMessage());
        }
    }

    /**
     * @param int $id
     * @param BaseItem $item
     * @throws DataSourceException
     */
    public function updateItem(int $id, BaseItem $item): void
    {
        $body = $this->serializer->serialize($item, JsonEncoder::FORMAT);

        try {
            $this->apiClient->patch($this->getApiUrl($id), $body);
        } catch (ApiException $e) {
            throw new DataSourceException($e->getMessage());
        }
    }

    /**
     * @param array|null $params
     * @return array
     * @throws DataSourceException
     */
    public function getItems(array $params = null): array
    {
        try {
            $content = $this->apiClient->get($this->getApiUrl(), $params);
        } catch (ApiException $e) {
            throw new DataSourceException($e->getMessage());
        }
        return $this->serializer->deserialize($content, Item::class . '[]', JsonEncoder::FORMAT);
    }

    /**
     * @param int|null $id
     * @return string
     */
    private function getApiUrl(int $id = null): string
    {
        $url = $this->apiHost . self::API_RESOURCE;

        return $id === null ? $url : $url . "/$id";
    }
}
