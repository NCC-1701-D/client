<?php
declare(strict_types=1);

namespace App\Service\Item;

use App\Exception\DataSourceException;

interface DeleteItemInterface
{
    /**
     * @param int $id
     * @throws DataSourceException
     */
    public function deleteItem(int $id): void;
}
