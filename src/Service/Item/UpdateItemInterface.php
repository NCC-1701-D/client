<?php
declare(strict_types=1);

namespace App\Service\Item;

use App\Entity\Item\BaseItem;
use App\Exception\DataSourceException;

interface UpdateItemInterface
{
    /**
     * @param int $id
     * @param BaseItem $item
     * @throws DataSourceException
     */
    public function updateItem(int $id, BaseItem $item): void;
}
