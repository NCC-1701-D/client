<?php
declare(strict_types=1);

namespace App\Service\ApiClient;

interface RequestOptionsComposerInterface
{
    /**
     * @param array $queryParams
     * @return array
     */
    public function composeGetOptions(array $queryParams = null): array;

    /**
     * @param string $body
     * @return array
     */
    public function composePostOptions(string $body): array;

    /**
     * @param string $body
     * @return array
     */
    public function composePatchOptions(string $body): array;

    /**
     * @return array
     */
    public function composeDeleteOptions(): array;
}
