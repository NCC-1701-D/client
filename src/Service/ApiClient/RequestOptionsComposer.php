<?php
declare(strict_types=1);

namespace App\Service\ApiClient;

class RequestOptionsComposer implements RequestOptionsComposerInterface
{
    /**
     * @param array $queryParams
     * @return array
     */
    public function composeGetOptions(array $queryParams = null): array
    {
        $options = [
            'headers' => ['accept' => 'application/json']
        ];

        if ($queryParams !== null) {
            $options['query'] = $queryParams;
        }

        return $options;
    }

    /**
     * @param string $body
     * @return array
     */
    public function composePostOptions(string $body): array
    {
        return [
            'headers' => [
                'accept' => 'application/json',
                'Content-Type' => 'application/json'
            ],
            'body' => $body
        ];
    }

    /**
     * @param string $body
     * @return array
     */
    public function composePatchOptions(string $body): array
    {
        return [
            'headers' => [
                'accept' => 'application/json',
                'Content-Type' => 'application/merge-patch+json'
            ],
            'body' => $body
        ];
    }

    /**
     * @return array
     */
    public function composeDeleteOptions(): array
    {
        return [
            'headers' => ['accept' => '*/*']
        ];
    }
}
