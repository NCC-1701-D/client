<?php
declare(strict_types=1);

namespace App\Service\ApiClient;

use App\Exception\ApiException;

interface ApiClientInterface
{
    /**
     * @param string $url
     * @param array $queryParams
     * @throws ApiException
     * @return string
     */
    public function get(string $url, array $queryParams = null): string;

    /**
     * @param string $url
     * @param string $body
     * @throws ApiException
     * @return string
     */
    public function post(string $url, string $body): string;

    /**
     * @param string $url
     * @param string $body
     * @throws ApiException
     * @return string
     */
    public function patch(string $url, string $body): string;

    /**
     * @param string $url
     * @throws ApiException
     * @return string
     */
    public function delete(string $url): string;
}
