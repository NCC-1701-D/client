<?php
declare(strict_types=1);

namespace App\Service\ApiClient;

use App\Entity\ApiClient\ApiRequest;
use App\Exception\ApiException;
use App\Factory\ApiClient\ApiRequestFactoryInterface;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiClient implements ApiClientInterface
{
    private HttpClientInterface $httpClient;
    private RequestOptionsComposerInterface $requestOptionsComposer;
    private ApiRequestFactoryInterface $apiRequestFactory;

    /**
     * @param HttpClientInterface $httpClient
     * @param RequestOptionsComposerInterface $requestOptionsComposer
     * @param ApiRequestFactoryInterface $apiRequestBuilder
     */
    public function __construct(HttpClientInterface $httpClient,
                                RequestOptionsComposerInterface $requestOptionsComposer,
                                ApiRequestFactoryInterface $apiRequestBuilder)
    {
        $this->httpClient = $httpClient;
        $this->requestOptionsComposer = $requestOptionsComposer;
        $this->apiRequestFactory = $apiRequestBuilder;
    }

    /**
     * @param string $url
     * @param array $queryParams
     * @return string
     * @throws ApiException
     */
    public function get(string $url, array $queryParams = null): string
    {
        $options = $this->requestOptionsComposer->composeGetOptions($queryParams);
        $apiRequest = $this->apiRequestFactory->buildGetApiRequest($url, $options);

        return $this->executeRequest($apiRequest);
    }

    /**
     * @param string $url
     * @param string $body
     * @return string
     * @throws ApiException
     */
    public function post(string $url, string $body): string
    {
        $options = $this->requestOptionsComposer->composePostOptions($body);
        $apiRequest = $this->apiRequestFactory->buildPostApiRequest($url, $options);

        return $this->executeRequest($apiRequest);
    }

    /**
     * @param string $url
     * @param string $body
     * @return string
     * @throws ApiException
     */
    public function patch(string $url, string $body): string
    {
        $options = $this->requestOptionsComposer->composePatchOptions($body);
        $apiRequest = $this->apiRequestFactory->buildPatchApiRequest($url, $options);

        return $this->executeRequest($apiRequest);
    }

    /**
     * @param string $url
     * @return string
     * @throws ApiException
     */
    public function delete(string $url): string
    {
        $options = $this->requestOptionsComposer->composeDeleteOptions();
        $apiRequest = $this->apiRequestFactory->buildDeleteApiRequest($url, $options);

        return $this->executeRequest($apiRequest);
    }

    /**
     * @param ApiRequest $apiRequest
     * @return string
     * @throws ApiException
     */
    private function executeRequest(ApiRequest $apiRequest): string
    {
        try {
            $response = $this->httpClient->request(
                $apiRequest->getMethod(),
                $apiRequest->getUrl(),
                $apiRequest->getOptions()
            );
            $statusCode = $response->getStatusCode();

            if($statusCode !== $apiRequest->getAllowedStatus()) {
                throw new ApiException("Status code: $statusCode");
            }

            return $response->getContent();
        } catch (ExceptionInterface $e) {
            throw new ApiException($e->getMessage());
        }
    }
}
