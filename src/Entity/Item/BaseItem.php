<?php
declare(strict_types=1);

namespace App\Entity\Item;

use Symfony\Component\Validator\Constraints as Assert;

class BaseItem
{
    /**
     * @Assert\NotBlank
     */
    protected string $name;

    /**
     * @Assert\GreaterThanOrEqual(0)
     */
    protected int $amount;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return BaseItem
     */
    public function setName(string $name): BaseItem
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return BaseItem
     */
    public function setAmount(int $amount): BaseItem
    {
        $this->amount = $amount;
        return $this;
    }
}
