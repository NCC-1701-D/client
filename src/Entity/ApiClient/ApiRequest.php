<?php
declare(strict_types=1);

namespace App\Entity\ApiClient;

class ApiRequest
{
    private string $url;
    private string $method;
    private array $options;
    private int $allowedStatus;

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return ApiRequest
     */
    public function setMethod(string $method): ApiRequest
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return ApiRequest
     */
    public function setUrl(string $url): ApiRequest
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param array $options
     * @return ApiRequest
     */
    public function setOptions(array $options): ApiRequest
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @return int
     */
    public function getAllowedStatus(): int
    {
        return $this->allowedStatus;
    }

    /**
     * @param int $allowedStatus
     * @return ApiRequest
     */
    public function setAllowedStatus(int $allowedStatus): ApiRequest
    {
        $this->allowedStatus = $allowedStatus;
        return $this;
    }
}
