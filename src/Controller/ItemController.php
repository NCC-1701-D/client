<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\Item\CreateItemInterface;
use App\Service\Item\DeleteItemInterface;
use App\Service\Item\UpdateItemInterface;
use Psr\Log\LoggerInterface;
use App\Exception\DataSourceException;
use App\Factory\Item\BaseItemFactoryInterface;
use App\Form\Type\Item\ItemType;
use App\Service\Item\ItemDataSourceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/item", name="item_")
 */
class ItemController extends AbstractController
{
    /**
     * @param ItemDataSourceInterface $itemService
     * @param LoggerInterface $logger
     * @return Response|RedirectResponse
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboard(ItemDataSourceInterface $itemService, LoggerInterface $logger): Response
    {
        $itemsThatAreInStock = null;
        $itemsThatAreNotInStock = null;
        $itemsThatAreMoreThan5 = null;
        $allItems = null;

        try {
            $itemsThatAreInStock = $itemService->getItemsThatAreInStock();
            $itemsThatAreNotInStock = $itemService->getItemsThatAreNotInStock();
            $itemsThatAreMoreThan5 = $itemService->getItemsThatAreMoreThan5();
            $allItems = $itemService->getItems();
        } catch (DataSourceException $e) {
            $logger->error('dashboard: ' . $e->getMessage());
            return $this->redirectToRoute('data_source_error');
        }

        return $this->render('item/dashboard.html.twig', [
            'items_that_are_in_stock' => $itemsThatAreInStock,
            'items_that_are_not_in_stock' => $itemsThatAreNotInStock,
            'items_that_are_more_than_5' => $itemsThatAreMoreThan5,
            'all_items' => $allItems
        ]);
    }

    /**
     * @param int $id
     * @param DeleteItemInterface $itemService
     * @param LoggerInterface $logger
     * @return RedirectResponse
     * @Route("/delete/{id}", name="delete", requirements={"id"="\d+"})
     */
    public function delete(int $id, DeleteItemInterface $itemService, LoggerInterface $logger): RedirectResponse
    {
        try {
            $itemService->deleteItem($id);
        } catch (DataSourceException $e) {
            $logger->error('delete: ' . $e->getMessage());
            return $this->redirectToRoute('data_source_error');
        }

        return $this->redirectToRoute('item_dashboard');
    }

    /**
     * @param BaseItemFactoryInterface $baseItemFactory
     * @return Response
     * @Route("/create", name="create", methods={"GET"})
     */
    public function create(BaseItemFactoryInterface $baseItemFactory): Response
    {
        $newItem = $baseItemFactory->createBaseItem();
        $form = $this->createForm(ItemType::class, $newItem);

        return $this->render('item/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param CreateItemInterface $itemService
     * @param BaseItemFactoryInterface $baseItemFactory
     * @param LoggerInterface $logger
     * @return Response|RedirectResponse
     * @Route("/create", name="create_process", methods={"POST"})
     */
    public function createProcess(Request $request,
                                  CreateItemInterface $itemService,
                                  BaseItemFactoryInterface $baseItemFactory,
                                  LoggerInterface $logger): Response
    {
        $newItem = $baseItemFactory->createBaseItem();
        $form = $this->createForm(ItemType::class, $newItem);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newItem = $form->getData();

            try {
                $itemService->createItem($newItem);
            } catch (DataSourceException $e) {
                $logger->error('create: ' . $e->getMessage());
                return $this->redirectToRoute('data_source_error');
            }

            return $this->redirectToRoute('item_dashboard');
        }

        return $this->render('item/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param int $id
     * @param ItemDataSourceInterface $itemService
     * @param LoggerInterface $logger
     * @return Response|RedirectResponse
     * @Route("/edit/{id}", name="edit", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function edit(int $id, ItemDataSourceInterface $itemService, LoggerInterface $logger): Response
    {
        $item = null;

        try {
            $item = $itemService->getItem($id);
        } catch (DataSourceException $e) {
            $logger->error('edit: ' . $e->getMessage());
            return $this->redirectToRoute('data_source_error');
        }
        $form = $this->createForm(ItemType::class, $item);

        return $this->render('item/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param int $id
     * @param Request $request
     * @param UpdateItemInterface $itemService
     * @param BaseItemFactoryInterface $baseItemFactory
     * @param LoggerInterface $logger
     * @return Response|RedirectResponse
     * @Route("/edit/{id}", name="edit_process", methods={"POST"}, requirements={"id"="\d+"})
     */
    public function editProcess(int $id,
                                Request $request,
                                UpdateItemInterface $itemService,
                                BaseItemFactoryInterface $baseItemFactory,
                                LoggerInterface $logger): Response
    {
        $item = $baseItemFactory->createBaseItem();
        $form = $this->createForm(ItemType::class, $item);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();

            try {
                $itemService->updateItem($id, $item);
            } catch (DataSourceException $e) {
                $logger->error('edit: ' . $e->getMessage());
                return $this->redirectToRoute('data_source_error');
            }

            return $this->redirectToRoute('item_dashboard');
        }

        return $this->render('item/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
