<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @return Response
     * @Route("/", name="homepage")
     */
    public function index(): Response
    {
        return $this->render('main/homepage.html.twig');
    }

    /**
     * @return Response
     * @Route("/error", name="data_source_error")
     */
    public function dataSourceError(): Response
    {
        return $this->render('main/data_source_error.html.twig');
    }
}
