<?php
declare(strict_types=1);

namespace App\Factory\ApiClient;

use App\Entity\ApiClient\ApiRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiRequestFactory implements ApiRequestFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function buildGetApiRequest(string $url, array $options): ApiRequest
    {
        return (new ApiRequest())
            ->setUrl($url)
            ->setMethod(Request::METHOD_GET)
            ->setOptions($options)
            ->setAllowedStatus(Response::HTTP_OK);
    }

    /**
     * @inheritDoc
     */
    public function buildPostApiRequest(string $url, array $options): ApiRequest
    {
        return (new ApiRequest())
            ->setUrl($url)
            ->setMethod(Request::METHOD_POST)
            ->setOptions($options)
            ->setAllowedStatus(Response::HTTP_CREATED);
    }

    /**
     * @inheritDoc
     */
    public function buildPatchApiRequest(string $url, array $options): ApiRequest
    {
        return (new ApiRequest())
            ->setUrl($url)
            ->setMethod(Request::METHOD_PATCH)
            ->setOptions($options)
            ->setAllowedStatus(Response::HTTP_OK);
    }

    /**
     * @inheritDoc
     */
    public function buildDeleteApiRequest(string $url, array $options): ApiRequest
    {
        return (new ApiRequest())
            ->setUrl($url)
            ->setMethod(Request::METHOD_DELETE)
            ->setOptions($options)
            ->setAllowedStatus(Response::HTTP_NO_CONTENT);
    }
}
