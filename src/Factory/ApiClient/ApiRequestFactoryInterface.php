<?php
declare(strict_types=1);

namespace App\Factory\ApiClient;

use App\Entity\ApiClient\ApiRequest;

interface ApiRequestFactoryInterface
{
    /**
     * @param string $url
     * @param array $options
     * @return ApiRequest
     */
    public function buildGetApiRequest(string $url, array $options): ApiRequest;

    /**
     * @param string $url
     * @param array $options
     * @return ApiRequest
     */
    public function buildPostApiRequest(string $url, array $options): ApiRequest;

    /**
     * @param string $url
     * @param array $options
     * @return ApiRequest
     */
    public function buildPatchApiRequest(string $url, array $options): ApiRequest;

    /**
     * @param string $url
     * @param array $options
     * @return ApiRequest
     */
    public function buildDeleteApiRequest(string $url, array $options): ApiRequest;
}
