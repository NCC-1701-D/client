<?php
declare(strict_types=1);

namespace App\Factory\Item;

use App\Entity\Item\BaseItem;

class FormBaseItemFactory implements BaseItemFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createBaseItem(): BaseItem
    {
        return (new BaseItem())
            ->setName('')
            ->setAmount(0);
    }
}
