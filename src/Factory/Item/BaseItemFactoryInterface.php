<?php
declare(strict_types=1);

namespace App\Factory\Item;

use App\Entity\Item\BaseItem;

interface BaseItemFactoryInterface
{
    /**
     * @return BaseItem
     */
    public function createBaseItem(): BaseItem;
}
